﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;


namespace ComicReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ComicBook cb;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OpenCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog("Select Comic Book");
            dialog.Filters.Add(new CommonFileDialogFilter("Comic Books", "*.cbz"));
            if(dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                cb = new ComicBook();
                cb.LoadArchive(dialog.FileName);
                ImageOut.Source = cb.getCurrentpage().image;
            }
        }
      

        private void LoadNext_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (cb != null && cb.currentPage+1 < cb.pages.Count) e.CanExecute = true;
            
        }

        private void LoadNext_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ImageOut.Source = cb.getNextPage().image;
        }

        private void LoadPrev_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (cb != null && cb.currentPage > 0) e.CanExecute = true;
        }

        private void LoadPrev_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
            ImageOut.Source = cb.getPreviousPage().image;
        }
    }
}
