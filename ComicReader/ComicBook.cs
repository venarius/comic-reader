﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Threading;
using System.Windows.Controls;
using Ionic.Zip;

namespace ComicReader
{
    class ComicBook
    {
        ZipFile zip;
        public int currentPage;
        public List<ComicBookPage> pages = new List<ComicBookPage>();
        

        public void LoadArchive(string archiveFile)
        {
            if(archiveFile.EndsWith(".cbz", StringComparison.Ordinal))
            {
                zip = ZipFile.Read(archiveFile);
                ReadPageList();
            }
            
        }

        private void ReadPageList()
        {
            foreach(ZipEntry entry in zip.Entries)
            {
                if (entry.IsDirectory) continue;
                if (!entry.FileName.EndsWith(".jpg", StringComparison.Ordinal)) continue;

                pages.Add(new ComicBookPage(entry.FileName));
            }
        }

        private BitmapImage LoadImageFromZIP(string filename)
        {
            if(zip[filename] != null)
            {
                ZipEntry file = zip[filename];
                var buffer = new byte[file.UncompressedSize];
                var zipStream = new MemoryStream(buffer);
                file.Extract(zipStream);

                using (MemoryStream sourceStream = new MemoryStream(buffer))
                {
                    return ConvertStreamToBitmap(sourceStream);
                }
            }
            return null;
        }



        private BitmapImage ConvertStreamToBitmap(Stream source)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = source;
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.EndInit();
            bi.Freeze();
            return bi;
        }

        private void CacheCleanup()
        {
            foreach(ComicBookPage page in pages)
            {
                if(pages.FindIndex(x => x==page)<currentPage-5 || pages.FindIndex(x => x == page) >currentPage+5)
                {
                    page.image = null;
                }
            }
        }

        private ComicBookPage LoadPage(int index)
        {
            CacheCleanup();
            ComicBookPage page = pages.ElementAt(index);
            if(page.image!=null)
            {
                return page;
            }else
            {
                page.image = LoadImageFromZIP(page.filename);
                return page;
            }
            
        }

        public ComicBookPage getCurrentpage()
        {
            if(pages.Count>0)
            {
                return LoadPage(currentPage);
            }
            return null;
        }

        public ComicBookPage getNextPage()
        {
            if(pages.Count()>currentPage+1)
            {
                currentPage++;
                return LoadPage(currentPage);
            }
            return null;
        }

        public ComicBookPage getPreviousPage()
        {
            if (currentPage>0)
            {
                currentPage--;
                return LoadPage(currentPage);
            }
            return null;
        }
    }

    
}
