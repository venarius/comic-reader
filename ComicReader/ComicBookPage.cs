﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ComicReader
{
    class ComicBookPage
    {
        public string filename;
        public BitmapImage image;

        public ComicBookPage(string filename, BitmapImage image)
        {
            this.filename = filename;
            this.image = image;
        }

        public ComicBookPage(string filename)
        {
            this.filename = filename;
        }
    }
}
